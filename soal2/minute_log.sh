#!/bin/bash

#file name
file=metrics_$(date + "%Y%m%d%H%M%S").log

#column name
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $file

#insert resource log
free -m | awk 'NR==2{printf $2 "," $3 ","  $4 ","  $5 ","  $6 "," $7 ","} NR==3 {printf $2 "," $3 "," $4 ","}' >> $file
du -sh /home/nex/ | awk '{print $2 "," $1}' >> $file

#revoke read, write, execute permission
chmod go-rwx $file
